import React, { useEffect, useState, useContext } from 'react';
import { Link, useLocation } from "react-router-dom";
import BlockchainContext from '../../lib/blockchain/blockchain-context'
import { SSO } from 'ft3-lib';
import { parse } from 'query-string';

const SSOSuccess = () => {
  const blockchain = useContext(BlockchainContext);
  const { search } = useLocation();
  const [processing, setProcessing] = useState(true);
  const [account, setAccount] = useState(null);
  const [session, setSession] = useState(null);
  const [error, setError] = useState(null);
  const [tx] = useState(search && parse(search).rawTx);

  useEffect(() => {
    async function verifyAndSendTx() {
      if (!tx) {
        setError('Invalid response');
        return
      }

      try {
        const sso = new SSO(blockchain);
        const [account, user] = await sso.finalizeLogin(tx)
        setAccount(account);
        setSession(blockchain.newSession(user));
      } catch (e) {
        console.error('Login error')
        setError(e.message)
      } finally {
        setProcessing(false);
      }
    }

    if (!account) {
      verifyAndSendTx()
    }
  }, []);

  if (processing) {
    return <h1>PROCESSING....</h1>
  }

  const logout = async () => {
    await new SSO(blockchain).logout();
    window.location.href = process.env.URL;
  }
  if (error) {
    return (
      <div>
        <h1>Error (maybe you reloaded this page?)</h1>
        <p>Error: {error}</p>
        <Link to="/">Back to home</Link>
      </div>
    )
  }
  return (
    <>
      <h1>Success</h1>
      <p>Account id: {account.id.toString("hex")} </p>
      <a onClick={logout} href="" >Back to home</a>
    </>
  )
}

export default SSOSuccess;