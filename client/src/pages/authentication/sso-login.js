import React, { useContext } from 'react';
import { SSO, User } from 'ft3-lib';
import BlockchainContext from '../../lib/blockchain/blockchain-context';
import { setStoredAccount } from '../../lib/account-storage';
import LoginButton from '../../component/chromia/ui/login-button';

const Login = () => {

  const blockchain = useContext(BlockchainContext)
  const login = () => {
    const successUrl = `${window.location.origin}/sso/success`;
    const cancelUrl = `${window.location.origin}`;

    const user = User.generateSingleSigUser();
    setStoredAccount({ user });

    new SSO(blockchain).initiateLogin(successUrl, cancelUrl);
  }

  return (
    <>
      <h1>Login</h1>
      <LoginButton dark={false} onClick={login} />
    </>
  )
}

export default Login;